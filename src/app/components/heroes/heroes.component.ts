import {Component, OnInit} from '@angular/core';
import {Hero} from "../../models/hero";
import {HeroService} from "../../services/hero.service";
import {MessageService} from "../../services/message.service";

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  selectedHero?: Hero;

  heroes: Hero[] = [];

  //Même principe d'injection de dépendance que spring
  constructor(private heroService: HeroService, private messageService: MessageService) {
  }

  ngOnInit(): void {
    //Hook d'initialisation
    this.getHeroes();
  }

  getHeroes() {
    this.heroService.getHeroes().subscribe(heroes => {
      this.heroes = heroes;
    })
  }

  add(name: string): void {
    name = name.trim(); //remove espace
    if (!name){ //Non null assertion
        return;
    }

    this.heroService.addHero({name} as Hero).subscribe(newHero =>{
      this.heroes.push(newHero); // Ajout dynamique dans le tableau
    })

  }

  delete(hero : Hero) : void {
    this.heroes = this.heroes.filter(h => h !== hero); //Mise à jour dynamique du tableau
    this.heroService.deleteHero(hero.id).subscribe();
  }


  // onSelect(hero : Hero){
  //   this.selectedHero = hero;
  //   this.messageService.add('Selected Heroes: ' + hero.name)
  // }


}
