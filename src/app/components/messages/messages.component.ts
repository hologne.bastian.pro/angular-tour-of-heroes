import { Component, OnInit } from '@angular/core';
import {MessageService} from "../../services/message.service";

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  //La dependance doit être mis public si on veut pouvoir s'en servir dans le html
  constructor(public messageService: MessageService) { }

  ngOnInit(): void {
  }

}
